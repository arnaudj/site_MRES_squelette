<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2017                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Gestion du formulaire de d'édition d'article
 *
 * @package SPIP\Core\Articles\Formulaires
 **/


include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 **/
function formulaires_editer_emploi_charger_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	$valeurs = formulaires_editer_objet_charger(
		'article',
		$id_article,
		$id_rubrique,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);

	$tab_mot_sql = sql_allfetsel("id_mot,titre","spip_mots","id_groupe=1");
	$tab_mot=array();
	foreach($tab_mot_sql as &$mot)
		$tab_mot[$mot['id_mot']]=$mot['titre'];
	$valeurs['mots_datas']= $tab_mot;
	$valeurs['tab_type_offre']= [
		37=>'Offres d’emploi',
		38=>'Offres de stages',
		142=>'Offres de mission service civique',

	];
	// il faut enlever l'id_rubrique car la saisie se fait sur id_parent
	// et id_rubrique peut etre passe dans l'url comme rubrique parent initiale
	// et sera perdue si elle est supposee saisie
	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui
 * ne représentent pas l'objet édité
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_emploi_identifier_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	return serialize(array(intval($id_article), $lier_trad));
}

/**
 * Choix par défaut des options de présentation
 *
 * @param array $row
 *     Valeurs de la ligne SQL d'un article, si connu
 * return array
 *     Configuration pour le formulaire
 */
function articles_edit_config($row) {
	global $spip_lang;

	$config = $GLOBALS['meta'];
	$config['lignes'] = 8;
	$config['langue'] = $spip_lang;

	$config['restreint'] = ($row['statut'] == 'publie');

	return $config;
}

/**
 * Vérifications du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Erreurs du formulaire
 **/
function formulaires_editer_emploi_verifier_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	// auto-renseigner le titre si il n'existe pas
	titre_automatique('titre', array('descriptif', 'chapo', 'texte'));
	// on ne demande pas le titre obligatoire : il sera rempli a la volee dans editer_texte si vide
	$erreurs = formulaires_editer_objet_verifier('article', $id_article, array('id_parent'));
	// si on utilise le formulaire dans le public
	if (!function_exists('autoriser')) {
		include_spip('inc/autoriser');
	}
	if (!isset($erreurs['id_parent'])
		and !autoriser('creerarticledans', 'rubrique', _request('id_parent'))
	) {
		$erreurs['id_parent'] = _T('info_creerdansrubrique_non_autorise');
	}

	return $erreurs;
}

/**
 * Traitements du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 **/
function formulaires_editer_emploi_traiter_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	// ici on ignore changer_lang qui est poste en cas de trad,
	// car l'heuristique du choix de la langue est pris en charge par article_inserer
	// en fonction de la config du site et de la rubrique choisie
	set_request('changer_lang');


	$res  = formulaires_editer_objet_traiter(
		'article',
		$id_article,
		$id_rubrique,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);




	if (isset($res['message_ok'])){

			if ($id_objet = intval($res['id_article'])
			and autoriser('instituer', 'article', $id_objet, '', array('statut' => 'publie'))
		) {

			include_spip('action/editer_objet');



				$chapo = _request('nom_structure');
				$texte='';
				if ($temp=_request('structure'))
					$texte.= PHP_EOL.$temp;
				if ($temp=_request('poste'))
					$texte.='{{{Description du poste}}}'.PHP_EOL.$temp;
				if ($temp=_request('competence'))
					$texte.='{{{Compétences requises}}}'.PHP_EOL.$temp;
				if ($temp=_request('formalite'))
					$texte.='{{{Comment répondre}}}'.PHP_EOL.$temp;

			objet_modifier('article', $id_objet, array('chapo'=>$chapo,'texte'=>$texte,'statut' => 'publie'));




				$tab_mot = _request('mots');
				include_spip('action/editer_mot');
				if (!empty($tab_mot)) {
				foreach($tab_mot as $mot){
					if(intval($mot)==0){
						$id_mot = mot_inserer(1);
						mot_modifier($id_mot,['titre'=>$mot]);
					}else{
						$id_mot=$mot;
					}

					mot_associer($id_mot, array('article' => $id_objet));
				}
				}
		}


	}
	return $res;
}
