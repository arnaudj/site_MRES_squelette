$(document).ready(function(){

    $('nav#nav a').each(function(){
        $(this).attr('data-hover',$(this).text());

    });

    $('#responsive-menu-button').sidr({
        name: 'sidr-main',
        source: '#nav .navbar-inner'
    });


    initialisation_modal();

    initialisation_modal_form();

    initialisation_modal_event();

});






function transformeULtoSELECT(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    $('ul.futur_select',conteneur).each(function() {
        var list = $(this);
        var select = $(document.createElement('select')).insertBefore($(this).hide());

        $('>li a', this).each(function() {
            var target = $(this).attr('target'),
                option = $(document.createElement('option'))
                    .appendTo(select)
                    .val(this.href)
                    .html($(this).html())
                    .click(function(){
                        if(target==='_blank') {
                            window.open($(this).val());
                        }
                        else {
                            window.location.href = $(this).val();
                        }
                    });
            if($(this).hasClass('on'))
                option.attr('selected','selected');

        });
        list.remove();
    });



}






function initialisation_modal(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    conteneur=$(conteneur);

    $('.btn-modal[data-modal]',conteneur).click(function(ev) {
        var selector = '#'+$(this).attr('data-modal');
        var titre = $(this).text();
        var id = $(this).attr('id')+'_modal';

        modal_form=$('#'+id);
        if (!$(modal_form).length) {
            $('body').append(genererModal(id,titre,'<p></p>','',true));
            modal_form=$('#'+id);
            $('.modal-title',modal_form).text(titre);
            $('.modal-body',modal_form).html($(selector));
            $(selector,modal_form).removeClass('hide');
            $('.modal-body',modal_form).append('<div class="clearfix"></div>')
        }

        $(modal_form).modal({show:true});
        return false;
    });

    $('a.btn-modal[href]',conteneur).click(function(ev) {
        var titre = $(this).attr('title');
        var id = $(this).attr('id')+'_modal';
        var href = $(this).attr('href');
        class_modal = '';
        if ($(this).hasClass('modal-large')){
            class_modal = 'modal-lg';
        }
        modal_form=$('#'+id);
        if (!$(modal_form).length) {
            $('body').append(genererModal(id,titre,'<p></p>','',true,class_modal));
            modal_form=$('#'+id);
            $('.modal-title',modal_form).text(titre);
            $('.modal-body',modal_form).load(href);
            $('.modal-body',modal_form).append('<div class="clearfix"></div>')
        }
        $(modal_form).modal({show:true});
        $(modal_form).on('hidden.bs.modal', function (e) {
            modal_form.remove();
        });
        return false;
    });

}

function initialisation_modal_event(conteneur) {

    if (conteneur == undefined)
        conteneur = 'body'

    conteneur = $(conteneur)


    $('a[data-event]', conteneur).click(function (ev) {


        modal_event = $('#modal_evt')
        if (!$(modal_event).length) {
            $('body').append('' +
                '<div class="modal fade" id="modal_evt" tabindex="-1" role="dialog" aria-labelledby="modal_evt_label">' +
                '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<h4 class="modal-title">Évenement</h4>' +
                '</div>' +
                '<div class="modal-body"></div>' +
                '</div><!-- /.modal-content -->' +
                '</div><!-- /.modal-dialog -->' +
                '</div><!-- /.modal -->')
            modal_event = $('#modal_evt')

        }

        $("#modal_evt .modal-body").load($(this).attr('data-event'), function (response, status, xhr) {
            if (status == "error") {
            } else {
                $('#modal_evt').modal('show');


                $(modal_event).modal({show: true});

            }
        });
        return false;
    });

}





function genererModal(id,titre,body,footer,close,class_cplt,header){

    if (class_cplt == undefined)
        class_cplt='';
    modal=  '<div id="'+id+'" class="modal fade" role="dialog" aria-labelledby="data'+id+'Label">' +
        '<div class="modal-dialog  '+class_cplt+'">' +
        '<div class="modal-content">' ;
    if (header != undefined) {
        modal+='<div class="modal-header">' ;
        if (close == true || close == undefined){
            modal+=  '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        }

        modal+='<h4 class="modal-title">'+titre+'</h4>' +
            '</div>';
    }
    modal+='<div class="modal-body">';
    if (body  == undefined || body ==''){
        modal+= '<p>Chargement...</p>' ;}
    else{
        modal+= body;}

    modal+='</div>' +
        '<div class="modal-footer">' +
        footer+
        '</div>' +
        '</div><!-- /.modal-content -->' +
        '</div><!-- /.modal-dialog -->' +
        '</div><!-- /.modal -->';

    return modal

}







function reagir_modal_form(data){


    $('.modal-body',modal_form).html(data);
    var options = {
        dataType:  'json',
        data: { json: '1'  },
        success: reagir_modal_form,
        clearForm: true
    };


    $('.modal-body form',modal_form).ajaxForm(options);


}



function initialisation_modal_form(conteneur){

    if (conteneur === undefined)
        conteneur='body';

    conteneur=$(conteneur);

    $('a.modal_form:not(.modalised)',conteneur).click(function(ev) {

        var href = $(this).attr('href');
        $(this).addClass('modalised');
        var titre = $(this).text();
        var id = 'modal_form';
        if ($(this).attr('id'))
            id = $(this).attr('id')+'_'+id;

        modal_form=$('#'+id);
        if (!$(modal_form).length) {
            $('body').append(genererModal(id,titre,'','',true));
            modal_form=$('#'+id);

            $(modal_form).on('hidden.bs.modal',function (e) {
                $('.modal-body',modal_form).html(' ')
            });
        }


        $('h4.modal-title ',modal_form).text(titre);
        $('.modal-body',modal_form).html();
        $('.modal-body:eq(0)',modal_form).eq(0).load(href,function() {
            var options = {
                beforeSubmit: modalform_beforeSubmit,
                success: reagir_modal_form,

            };
            $('.modal-body form',modal_form).ajaxForm(options);
        });
        $(modal_form).attr("disabled","disabled");
        $(modal_form).modal({show:true});
        return false;
    });

}



function modalform_beforeSubmit(formData, jqForm, options) {
    $(':submit',jqForm).attr('disabled','disabled');
    //console.log(formData);

    return true;
}

