// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var googleWebFonts = require('gulp-google-webfonts');
var path = require('path');

var config = {
    nodeDir: './node_modules/',
    bootstrapDir: './node_modules/bootstrap-sass/assets/',
    dir: './sources/',
    publicDir: './'
};


// Lint Task
gulp.task('lint', function () {
    return gulp.src(config.dir + 'js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});




gulp.task('build-datepicker', function() {
    return gulp.src([
        config.nodeDir + 'bootstrap-datepicker/less/datepicker3.less'


    ])
        .pipe(less({
            paths: [ config.dir+'less',config.dir+'less',config.nodeDir+'bootstrap-datepicker/less']
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('datepicker.css'))
        .pipe(gulp.dest(config.nodeDir +'bootstrap-datepicker/css'));
});



gulp.task('sass', function () {
    return gulp.src([

            config.dir + 'scss/bootstrap.scss',
            config.dir + 'css/bootstrap.scss',
            config.nodeDir + 'font-awesome/css/font-awesome.css',
            config.nodeDir+'select2/dist/css/select2.css',
            config.nodeDir + 'sidr/dist/stylesheets/jquery.sidr.light.css',
            config.dir + 'scss/animation.scss',
            config.dir + 'scss/perso.scss'
        ])
        .pipe(sass(
            {
                includePaths: [config.bootstrapDir + 'stylesheets/']
            }
        ).on('error', sass.logError))
        //   .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('perso.css'))
        .pipe(gulp.dest(config.publicDir + '/css'));
});





gulp.task('build-flag', function() {
    return gulp.src([
        config.dir+'/less/flag-icon.less'


    ])
        .pipe(less({
            paths: [ config.dir+'less',config.nodeDir+'flag-icon-css/less']
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('flag.css'))
        .pipe(gulp.dest(config.publicDir+'/css'));
});


gulp.task('copy-flag', function() {
    return  gulp.src([
        config.nodeDir+'flag-icon-css/flags/**/*.svg'
    ])
        .pipe(gulp.dest(config.publicDir+'/flags'));

});

gulp.task('icons', function() {
    gulp.src([
            config.nodeDir +'font-awesome/fonts/*.{ttf,woff,woff2,eot,svg}',
         ]
    )
        .pipe(gulp.dest(config.publicDir+'fonts'));
    return gulp.src([
            config.nodeDir +'bootstrap-sass/assets/fonts/bootstrap/*.{ttf,woff,woff2,eot,svg}'
        ]
    )
        .pipe(gulp.dest(config.publicDir+'fonts/bootstrap/'));
});


gulp.task('fonts', function () {
    return gulp.src(config.publicDir+'fonts.list')
        .pipe(googleWebFonts({}))
        .pipe(gulp.dest(config.publicDir+'fonts'));
});




// Concatenate & Minify JS
gulp.task('scripts', function () {
    return gulp.src([
        config.nodeDir + 'sidr/dist/jquery.sidr.js',
        config.nodeDir + 'isotope-layout/dist/isotope.pkgd.js',
        config.nodeDir +'select2/dist/js/select2.js',
        config.nodeDir +'select2/dist/js/select2.full.js',
        config.nodeDir +'select2/dist/js/i18n/fr.js',
        config.nodeDir + 'bootstrap-datepicker/js/bootstrap-datepicker.js',
        config.nodeDir + 'bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js',
        config.nodeDir + 'bootstrap-timepicker/js/bootstrap-timepicker.js',
        config.nodeDir +'velocity-animate/velocity.js',
        config.nodeDir +'velocity-animate/velocity.ui.js',
        config.dir + '/js/perso.js'
    ])
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});


gulp.task('scripts_bas_de_page', function () {


    return gulp.src([
        config.bootstrapDir + '/javascripts/bootstrap.js',
        config.dir+'/js/lunr/mustache.js',
        config.dir+'/js/lunr/elasticlunr.min.js',
        config.dir+'/js/lunr/lunr.stremmer.support.min.js',
        config.dir+'/js/lunr/lunr.fr.min.js',
        config.dir+'/js/lunr/lunr.js'
    ])
        .pipe(concat('scripts_footer.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.publicDir + '/js'));
});






// Default Task
gulp.task('default', ['sass', 'scripts', 'scripts_bas_de_page'], function () {


    gulp.watch(config.dir + '/js/*.js', function () {
        gulp.run('lint', 'scripts');
    });


    gulp.watch(config.dir + '/scss/**/*.scss', ['sass']);
});



