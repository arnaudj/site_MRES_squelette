<?php



function balise_REDIRECT($p)
{
	$url = interprete_argument_balise (1, $p);
	$p->code = "redirect($url)";
	$p->type = 'php';
  return $p;
}

function redirect($url="redirects") {
  include_spip('inc/headers');
  redirige_par_entete($url);
}








function lunr_data()
{

	$rubriques = recuperer_fond('liste/lunr_rubrique');

	$res= sql_query('SELECT id_rubrique,titre,GROUP_CONCAT(id_mot SEPARATOR \':\') as id_mots FROM spip_rubriques r LEFT JOIN spip_mots_liens m on  m.objet=\'rubrique\' and id_objet=id_rubrique where r.id_rubrique IN('.$rubriques.') group by id_rubrique, titre');

	$tab_mots_sql = sql_allfetsel('id_mot, titre', 'spip_mots');
	$tab_mots = array();
	foreach ($tab_mots_sql as $mots) {
		$tab_mots[$mots['id_mot']] = $mots['titre'];
	}

	while ($d = sql_fetch($res)) {
		$tab_id_mot = explode(':', $d['id_mots']);
		foreach ($tab_id_mot as $id_mot) {
			$d['mots'][] = $tab_mots[$id_mot];
		}
		$d['mots'] = implode(', ', $d['mots']);
		$d['titre'] = supprimer_numero($d['titre']);
		$d['nature'] = 'rubrique';
		$d['id'] = $d['id_rubrique'];
		unset($d['id_rubrique']);
		unset($d['id_mots']);
		$data[] = $d;

	}


	$tab_mots_sql = sql_allfetsel('id_mot, titre', 'spip_mots');
	$tab_mots = array();
	foreach ($tab_mots_sql as $mots) {
		$tab_mots[$mots['id_mot']] = $mots['titre'];
	}

	$mysql = function_exists('req_mysql_dist');

	$articles = trim(recuperer_fond('liste/lunr_article'));
	if(!empty($articles)){
		$res= sql_query('SELECT id_article,titre,GROUP_CONCAT(id_mot'.($mysql ?' SEPARATOR ':',').'\':\') as id_mots FROM spip_articles r LEFT JOIN spip_mots_liens m on  m.objet=\'article\' and id_objet=id_article where r.id_article IN('.$articles.')  group by id_article, titre');

		while ($d = sql_fetch($res)) {
			$tab_id_mot = explode(':', $d['id_mots']);
			foreach ($tab_id_mot as $id_mot) {
				$d['mots'][] = $tab_mots[$id_mot];
			}
			$d['mots'] = implode(', ', $d['mots']);
			$d['titre'] = supprimer_numero($d['titre']);
			$d['nature'] = 'Article';
			$d['id'] = $d['id_article'];
			unset($d['id_article']);
			unset($d['id_mots']);
			$data[] = $d;

		}
	}



	$res= sql_query('SELECT id_article,titre,GROUP_CONCAT(id_mot'.($mysql ?' SEPARATOR ':',').'\':\') as id_mots FROM spip_evenements r LEFT JOIN spip_mots_liens m on  m.objet=\'evenement\' and id_objet=id_article where date_fin > DATE_SUB(NOW(),INTERVAL 10 DAY)  group by id_article, titre');

	while ($d = sql_fetch($res)) {
		$tab_id_mot = explode(':', $d['id_mots']);
		foreach ($tab_id_mot as $id_mot) {
			$d['mots'][] = $tab_mots[$id_mot];
		}
		$d['mots'] = implode(', ', $d['mots']);
		$d['titre'] = supprimer_numero($d['titre']);
		$d['nature'] = 'agenda';
		$d['id'] = $d['id_article'];
		unset($d['id_article']);
		unset($d['id_mots']);
		$data[] = $d;

	}


	$tab_id = trim(recuperer_fond('liste/lunr_association'));
	if(!empty($tab_id)){
		$res= sql_query('SELECT id_association,nom,sigle,GROUP_CONCAT(id_mot'.($mysql ?' SEPARATOR ':',').'\':\') as id_mots FROM spip_associations r LEFT JOIN spip_mots_liens m on  m.objet=\'association\' and id_objet=id_association where r.id_association IN('.$tab_id.')  group by id_association, nom, sigle');

		while ($d = sql_fetch($res)) {
			$tab_id_mot = explode(':', $d['id_mots']);
			foreach ($tab_id_mot as $id_mot) {
				$d['mots'][] = $tab_mots[$id_mot];
			}
			$d['mots'] = implode(', ', $d['mots']);
			$d['titre'] = supprimer_numero($d['nom'].(!empty($d['sigle'])? ' ( '.$d['sigle'].' )':''));
			$d['nature'] = 'association';
			$d['id'] = $d['id_association'];
			unset($d['id_association']);
			unset($d['id_mots']);
			$data[] = $d;

		}
	}




	return $data;
}




function  balise_LUNR_DATA($p)
{
	$p->code = "lunr_data()";
	return $p;
}




