# Pré-requis

Avoir un serveur LAMP avec un spip installé en version 3.1.
Pour le téléchargement et la gestion des dépendances (librairie js), il vous 
faut avoir installer nodejs sur votre systeme.

# Installation en local

```
git clone git@framagit.org:mres-web/site_MRES_squelette.git
cd squelettes
npm install
```
Puis voila un petit bash pour installer tous les plugins spip qui vont bien
```
#!/bin/bash
paquets=(ancres_douces blocsdepliables comments-300 compositions_v3 crayons modeles_facebook_s3 inserer_modeles_v1 magnet menus_1 minibando-dev noie nospam numerotation 'pages' 'pays_v3' 'saisies_v2' 'socialtags' 'sociaux' 'spip-bonux-3' 'verifier' 'yaml' 'z-core')
for i in ${paquets[@]}; do
	wget http://files.spip.org/spip-zone/${i}.zip
	unzip -o ${i}.zip -d plugins/auto
	rm ${i}.zip
done
exit 0
```
ce script est à executer à la racine du site

Ensuite installer la copie la base de données, et vous rendre sur l'adresse local http:127.0.0.1/mres-web/ecrire pour terminer l'installation de spip.

et voila, il ne reste qu'à activer les plugins dans l'espace privé de spip.

# Utilisation des librairies et outils (gulp et ses modules)

Lors du développement, quand vous souhaitez modifier les feuilles de styles ou 
les js, n'oubliez pas de lancer gulp dans une console à part,
il s'occupera de compiler les fichiers à chaque modifications des 
fichiers sources.

Pour savoir ce que fait exactement gulp, il suffit de lire
dans le fichier `squelettes/Gulpfile.js`



# Les plugins à activer à minima

- agenda
- calendriermini
- comments
- compositions
- crayons
- media
- menus
- noie
- pages
- saisies
- spip_bonux
- typoenluminee
- verifier
- zcore

