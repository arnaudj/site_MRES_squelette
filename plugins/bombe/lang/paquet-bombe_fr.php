<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-bombe
// Langue: fr
// Date: 12-10-2012 13:31:19
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
	'bombe_description' => 'Le plugin "La bombe" permet d\'attacher trs facilement des mots clés à la volée" sur n\'importe quel élément les acceptant (articles, forums, etc, et plus encore si vous avez le plugin Mots Objets ou si vous savez programmer).',
	'bombe_slogan' => 'Le plugin "La bombe" permet d\'attacher trs facilement des mots clés à la volée" sur n\'importe quel élément les acceptant (articles, forums, etc, et plus encore si vous avez le plugin Mots Objets ou si vous savez programmer)',
);
?>