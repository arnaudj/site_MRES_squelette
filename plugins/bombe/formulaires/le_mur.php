<?php
#---------------------------------------------------#
#  Plugin  : Étiquettes                             #
#  Auteur  : RastaPopoulos                          #
#  Licence : GPL                                    #
#------------------------------------------------------------------------------------------------------#
#  Documentation : http://www.spip-contrib.net/Plugin-Etiquettes                                       #
#                                                                                                      #
#  Définition de la balise #FORMULAIRE_ETIQUETTES                                                      #
#------------------------------------------------------------------------------------------------------#

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) {
    return;
}


function formulaires_le_mur_charger_dist($type_objet, $id_objet)
{

    $valeurs = compact("type_objet", "id_objet");
    $tab_tags = array();
    $id_groupe = lire_config('bombe/id_groupe', 1);

    $reponse = sql_allfetsel('id_groupe,id_mot', array('mots' => 'spip_mots'),
         'id_groupe='.intval($id_groupe));
    $tab_id_mot=array();
    foreach ($reponse as $gr) {  $tab_id_mot[] = intval($gr['id_mot']);    }


    if ($type_objet and $id_objet) {

        $reponse = sql_allfetsel('id_mot as id_mot',
            'spip_mots_liens ',
             'id_mot IN (' . implode(',', $tab_id_mot) . ') AND '.
             'objet ='. sql_quote($type_objet).' AND '.
             'id_objet='. $id_objet
            );
        foreach ($reponse as $gr) {  $tab_tags[] = intval($gr['id_mot']); }
    }
    $valeurs['tab_tags'] = $tab_tags;
    $valeurs['name'] = "cherche_mot";

    return $valeurs;


}

function formulaires_le_mur_verifier_dist($type_objet, $id_objet)
{
    $erreurs = array();
    return $erreurs;

}

function formulaires_le_mur_traiter_dist($type_objet, $id_objet)
{

    //$identifiant = etiquettes_produire_id($groupe, $type_objet, $id_objet);
    $groupe = lire_config('bombe/id_groupe');

    // On récupère les tags
    $tab_id =trim(_request('cherche_mot'));
    $tab_id = explode(',',$tab_id);

    include_spip('inc/bombe-machine');
    bombe_remplacer_mot(
        $tab_id,
        $id_objet,
        $type_objet,
        $groupe,
        true
    );

    // On dit qu'il faut recalculer tout vu qu'on a changé
    include_spip("inc/invalideur");
    suivre_invalideur("1");

    // Relance la page
    include_spip('inc/headers');
    redirige_formulaire(self());

}
