<?php
#---------------------------------------------------#
#  Plugin  : LaBombe                                #
#  Auteur  : guillaume Wauquier                     #
#  Licence : GPL                                    #
#--------------------------------------------------------------- -#
#  Documentation : http://www.spip-contrib.net/Plugin-LaBombe     #
#-----------------------------------------------------------------#




// lister les exec ou apparait le mur à taguer et le type correspondant
// peut etre etendu par des plugins
$GLOBALS['la_bombe_exec']['naviguer'] = 'rubrique';
$GLOBALS['la_bombe_exec']['article'] = 'article';
$GLOBALS['la_bombe_exec']['rubrique'] = 'rubrique';
$GLOBALS['la_bombe_exec']['evenement'] = 'evenement';
$GLOBALS['la_bombe_exec']['association'] = 'association';
$GLOBALS['la_bombe_exec']['sites'] = 'site';
$GLOBALS['la_bombe_exec']['auteur_infos'] = 'auteur';



function bombe_affiche_milieu($flux){


	include_spip('inc/layer');
	include_spip('inc/presentation');

	$exec = $flux['args']['exec'];
	if (isset($GLOBALS['la_bombe_exec'][$exec])){
		$type = $GLOBALS['la_bombe_exec'][$exec];
		$_id = id_table_objet($type);
		if ($id = $flux['args'][$_id])
		{
		$autoriser = autoriser("associermots",$type,$id);
		if($autoriser){
			$nb=sql_countsel("spip_mots_liens", 'objet='.sql_quote($type).' AND id_objet='.$id);
			$deplier = ($nb > 0);
			$ids = "form_bombe";
			$bouton = bouton_block_depliable(strtoupper(_T('bombe:mot_clef_thematique')), $deplier, 'lemur');
			$flux['data'] .= debut_cadre_enfonce(chemin_image('bombe-24.png','images/'),true,'',$bouton);
			$flux['data'] .= debut_block_depliable($deplier,"lemur");
			$flux['data'] .= recuperer_fond('prive/editer/lemur',array_merge($_GET,array('type'=>$type,'id'=>$id)));
			$flux['data'] .= fin_block();
			$flux['data'] .= fin_cadre();
		}
		}

	}
	return $flux;
}




function bombe_header_prive($flux){
	return bombe_insert_javascript($flux);
}


function bombe_insert_javascript($flux){

	$labombe = find_in_path('javascript/la_bombe.js');
	$css = find_in_path('css/la_bombe.css');
	$flux .= '<link rel="stylesheet" type="text/css" media="all" href="'.$css.'" />
				<script type="text/javascript" src="'.$labombe.'"></script>';

	return $flux;
}

