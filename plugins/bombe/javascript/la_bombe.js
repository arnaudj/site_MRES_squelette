//#---------------------------------------------------#
//#  Plugin  : La bombe                               #
//#  Auteur  : Guillaume Wauquier                     #
//#  Licence : GPL                                    #
//#------------------------------------------------------------------------------------------------------#
//#  Documentation : http://www.spip-contrib.net/Plugin-La-bombe                                         #
//#------------------------------------------------------------------------------------------------------#


function liste_mot_cle(){

	tab_tag=[];

	$('#lemur .tag.selected').each(function(){
		console.log($(this).attr('data-value'));
		tab_tag.push($(this).attr('data-value'));
	});
	$('#cherche_mot').val(tab_tag.join(','));
}


function poser_la_bombe()
{
	$('#lemur ul ul').hide();
	$('#nuage_tags li.selected').parents('ul').show();

	$('#nuage_tags .tag').click(function(event){
		event.stopPropagation();
		if ($(this).hasClass('selected')){
			$(this).removeClass('selected')
		}
		else {
			if($(this).has('ul')){
				$('>ul',this).slideDown();
			}
			$(this).addClass('selected')
		}
		liste_mot_cle();
	});

}




