<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_association' => 'Ajouter cette association',

	// C
	'champ_activite_label' => 'activite',
	'champ_adresse_label' => 'Adresse',
	'champ_association_label' => 'objet associatif',
	'champ_cp_label' => 'Cp',
	'champ_description_explication' => '2',
	'champ_description_label' => 'description',
	'champ_document_label' => 'document',
	'champ_email_label' => 'Email',
	'champ_facebook_label' => 'Facebook',
	'champ_fax_label' => 'Fax',
	'champ_historique_label' => 'historique',
	'champ_informations_label' => 'informations',
	'champ_nom_label' => 'Nom',
	'champ_page_label' => 'Page',
	'champ_permanence_label' => 'Permanence',
	'champ_sigle_label' => 'Sigle',
	'champ_syndication_label' => 'syndication',
	'champ_tel_label' => 'Téléphone',
	'champ_url_site_label' => 'URL du site internet',
	'champ_ville_label' => 'Ville',
	'confirmer_supprimer_association' => 'Confirmez-vous la suppression de cette association ?',

	// I
	'icone_creer_association' => 'Créer une association',
	'icone_modifier_association' => 'Modifier cette association',
	'info_1_association' => 'Une association',
	'info_associations_auteur' => 'Les associations de cet auteur',
	'info_aucun_association' => 'Aucune association',
	'info_nb_associations' => '@nb@ associations',

	// R
	'retirer_lien_association' => 'Retirer cette association',
	'retirer_tous_liens_associations' => 'Retirer tous les associations',

	// S
	'supprimer_association' => 'Supprimer cette association',

	// T
	'texte_ajouter_association' => 'Ajouter une association',
	'texte_changer_statut_association' => 'Cette association est :',
	'texte_creer_associer_association' => 'Créer et associer une association',
	'texte_definir_comme_traduction_association' => 'Cette association est une traduction de l\'association numéro :',
	'titre_association' => 'Association',
	'titre_associations' => 'Associations',
	'titre_associations_rubrique' => 'Associations de la rubrique',
	'titre_langue_association' => 'Langue de cette association',
	'titre_logo_association' => 'Logo de cette association',
	'titre_objets_lies_association' => 'Liés à cette association',


);
