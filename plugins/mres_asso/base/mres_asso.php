<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Annuaire des associations
 * @copyright  2017
 * @author     Guillaume Wauquier
 * @licence    GNU/GPL
 * @package    SPIP\Mres_asso\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function mres_asso_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['associations'] = 'associations';
	$interfaces['tables_jointures']['spip_breves'][]= 'associations_liens';
	$interfaces['tables_jointures']['spip_evenements'][]= 'associations_liens';
	$interfaces['tables_jointures']['spip_rubriques'][]= 'associations_liens';
	$interfaces['tables_jointures']['spip_articles'][]= 'associations_liens';
	$interfaces['table_des_traitements']['OBJET_ASSOCIATIF']['associations']= _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['DESCRIPTION']['associations']= _TRAITEMENT_RACCOURCIS;
	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function mres_asso_declarer_tables_objets_sql($tables) {

	$tables['spip_associations'] = array(
		'type' => 'association',
		'principale' => 'oui',
		'field'=> array(
			'id_association'     => 'bigint(21) NOT NULL',
			'sigle'              => 'mediumtext NOT NULL',
			'nom'                => 'text NOT NULL',
			'adresse'            => 'text NOT NULL',
			'cp'                 => 'int(5) NOT NULL DEFAULT "0"',
			'ville'              => 'mediumtext NOT NULL',
			'page'               => 'date NOT NULL',
			'tel'                => 'mediumtext NOT NULL',
			'fax'                => 'mediumtext NOT NULL',
			'email'              => 'mediumtext NOT NULL',
			'url_site'           => 'mediumtext NOT NULL',
			'facebook'           => 'mediumtext NOT NULL',
			'permanence'         => 'mediumtext NOT NULL',
			'objet_associatif'   => 'mediumtext NOT NULL DEFAULT ""',
			'activite'           => 'date NOT NULL DEFAULT "0000-00-00"',
			'historique'         => 'mediumtext NOT NULL DEFAULT ""',
			'informations'       => 'text NOT NULL DEFAULT ""',
			'description'        => 'mediumtext NOT NULL DEFAULT ""',
			'document'           => 'mediumtext NOT NULL DEFAULT ""',
			'syndication'        => 'mediumtext NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_association',
			'KEY statut'         => 'statut',
		),
		'titre' => 'nom AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => array('sigle', 'nom', 'adresse', 'cp', 'ville', 'tel', 'fax', 'email', 'url_site', 'facebook', 'permanence', 'objet_associatif', 'historique', 'informations', 'description', 'document', 'syndication'),
		'champs_versionnes' => array('sigle', 'nom', 'adresse', 'cp', 'ville', 'tel', 'fax', 'email', 'url_site', 'facebook', 'permanence', 'objet_associatif', 'historique', 'informations', 'description', 'document', 'syndication'),
		'rechercher_champs' => array("nom" => 10,'sigle'=>10, "historique" => 2),
		'tables_jointures'  => array( 'associations_liens'),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'association:texte_changer_statut_association',


	);


	//-- Jointures ----------------------------------------------------
	$tables['spip_articles']['tables_jointures'][] = 'associations_liens';
	$tables['spip_evenements']['tables_jointures'][] = 'associations_liens';
	$tables['spip_rubriques']['tables_jointures'][] = 'associations_liens';
	$tables['spip_breves']['tables_jointures'][] = 'associations_liens';

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function mres_asso_declarer_tables_auxiliaires($tables) {

	$tables['spip_associations_liens'] = array(
		'field' => array(
			'id_association'     => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu'              => 'VARCHAR(6) DEFAULT "" NOT NULL'


		),
		'key' => array(
			'PRIMARY KEY'        => 'id_association,id_objet,objet',
			'KEY id_association' => 'id_association',
		)
	);

	return $tables;
}
